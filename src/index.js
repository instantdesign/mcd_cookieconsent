'use strict';
import Cookies from 'js-cookie'
import './index.css'

const cookieName = 'cookieconsent_status'
const cookieApp = 'mcd_inapp'
const domain = process.env.NODE_ENV === 'production' ? '.mcdonalds.at' : 'localhost'
var scripts = []

function docReady(fn) {
  if (document.readyState === "complete" || document.readyState === "interactive") {
    setTimeout(fn, 1);
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

function enableScripts() {
	for (var i = 0; i < scripts.length; i++) {
    if (typeof(scripts[i]) === 'function') {
      scripts[i]()
    } else if (typeof(scripts[i]) === "string") {
      eval(scripts[i])
    } else if (scripts[i] && scripts[i].nodeType === 1) {
      scripts[i].type = "text/javascript"
      if (scripts[i].dataset.src) {
        scripts[i].src = scripts[i].dataset.src
      } else {
        eval(scripts[i].innerHTML)
      }
    }
	}
}

function inApp() {
  return Cookies.get(cookieApp) === "1"
}

function showPopup() {
  if (inApp()) {
    return
  }
	document.body.classList.add('cc-open')
	var div = document.createElement('div');
	div.classList.add('cc-bg')

	div.innerHTML =
`<div class="cc-box">
	<span class="cc-header">Privatsphäre-Einstellungen verwalten</span>
	<div id="cookieconsent:desc" class="cc-message">
		<p>
			Unsere Website verwendet Cookies, um Ihnen beste Erfahrungen zu ermöglichen. Diese Cookies können sich auf Sie, Ihre Präferenzen oder Ihr Gerät beziehen, wobei Sie in der Regel nicht direkt identifizierbar sind, jedoch eine stärker personalisierte Interneterfahrung erleben können. Einerseits handelt es sich hierbei um Cookies, die für die Funktionalität der Website zwingend erforderlich und daher automatisch aktiviert sind und bei Aufruf der Website gespeichert werden. Darüber hinaus verwenden wir mit Ihrer Einwilligung aber auch andere Technologien, die es uns ermöglichen, ein Profil Ihrer Interessen aufzubauen und Ihnen relevante Werbung auf anderen Websites zu zeigen. Diese Einwilligung können Sie jederzeit in den Cookie-Einstellungen widerrufen.
		</p>
		<p>
			Nähere Details, wie wir Ihre Daten verarbeiten und welche Cookies und andere Technologien wir verwenden, entnehmen Sie bitte unserer
			<a href="https://www.mcdonalds.at/datenschutz" target="_blank">Datenschutzerklärung</a>.
		</p>
	</div>
	<div class="cc-buttons">
		<a aria-label="allow cookies" role=button tabindex="0"  class="cc-btn cc-allow">Nutzung aller Cookies aktivieren</a>
		<a aria-label="deny cookies" role=button tabindex="0" class="cc-btn cc-deny">Website mit erforderlichen Cookies nutzen</a>
	</div>
</div>`

	function removeDiv() {
		document.body.removeChild(div)
		// document.body.classList.add('cc-closed')
		document.body.classList.remove('cc-open')
	}

	div.querySelector('.cc-allow').addEventListener('click', function () {
		removeDiv()
		Cookies.set(cookieName, 'allowed', { expires: 365, domain })
		enableScripts()
	})
	div.querySelector('.cc-deny').addEventListener('click', function () {
    removeDiv()
    var reload = Cookies.get(cookieName) === 'allowed'
		Cookies.set(cookieName, 'denied', { expires: 365, domain })
    if (reload) {
      history.go(0)
    }
	})
  // close on clicking the white bg
	// div.addEventListener('click', function (e) {
	// 	if (e.target === div) removeDiv()
	// })
	document.body.appendChild(div);
}

docReady(function() {
	scripts = document.querySelectorAll('script[type="text/plain"]')
	if (scripts.length) {
    var cookie = Cookies.get(cookieName)
		if (inApp() || cookie === 'allowed') {
			enableScripts()
		} else if (cookie !== 'denied') {
			showPopup()
		}
		if (document.querySelector('.cc-click-cookie-settings')) {
			document.querySelector('.cc-click-cookie-settings').addEventListener('click', function (event) {
				event.preventDefault()
				window.scrollTo(0, 0)
				showPopup()
			})
		}
	}
})

global.changeCookieUsage = showPopup
