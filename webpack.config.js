const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = (env, argv) => {
  var production = argv.mode === 'production'

  return {
    devtool: production ? false : 'source-map',
    module:{
      rules:[
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', { useBuiltIns: "usage", corejs: 3 }]
              ]
            }
          }
        },
        {
          test:/\.css$/,
          use:[
            'style-loader',
            { loader: "css-loader", options: { sourceMap: !production} },
            { loader: 'postcss-loader', options: { sourceMap: !production} }
          ]
        }
     ]
    },
    optimization: {
      minimizer: [
        new TerserJSPlugin({}),
        new OptimizeCSSAssetsPlugin({})
      ],
    },
  }
};
